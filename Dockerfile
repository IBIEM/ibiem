# Copyright (c) Jupyter Development Team.
# Distributed under the terms of the Modified BSD License.

# This dockerfile is derived from various Jupyter Dockerfiles
FROM jupyter/r-notebook:54838ed4acb1

MAINTAINER Josh Granek <josh@duke.edu>

USER root
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    fastqc default-jre \
    qiime \
    && apt-get clean && \
    rm -rf /var/lib/apt/lists/*

RUN echo "deb http://ftp.debian.org/debian jessie-backports main" >  /etc/apt/sources.list.d/backports.list && \
    apt-get update &&  \
    apt-get -t jessie-backports install -y --no-install-recommends \
    less \
    make \
    git \
    libxml2-dev \
    libgsl0-dev \
    bwa \
    samtools \
    seqtk \
    parallel \
    ssh \
    rsync \
    tophat \
    rna-star \
    ea-utils \
    file \
    aptitude \
    nano \
    && apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Install nbdime for jupyter notebook differences
RUN pip install nbdime

### sra-toolkit
## apt-get -q -y  install sra-toolkit
# RUN wget http://ftp-trace.ncbi.nlm.nih.gov/sra/sdk/2.4.3/sratoolkit.2.4.3-ubuntu64.tar.gz; \
#     tar -zxf sratoolkit.2.4.3-ubuntu64.tar.gz; \
#     cp -r sratoolkit.2.4.3-ubuntu64/bin/*  /usr/local/bin/; \
#     rm sratoolkit.2.4.3-ubuntu64.tar.gz; \
#     rm -r sratoolkit.2.4.3-ubuntu64; \
#     vdb-config --restore-defaults; \
#     echo "test sra-toolkit with the following command:"; \
#     echo "fastq-dump -X 5 -Z SRR390728"

RUN Rscript -e "install.packages(pkgs = c('pwr','RColorBrewer','GSA',\
    'dendextend','pheatmap','cgdsr','ggplot2','dplyr','ape','scales',\
    'grid','ecodist','optparse','biom'), \
    repos='https://cran.revolutionanalytics.com/', \
    dependencies=TRUE)"
RUN Rscript -e "source('https://bioconductor.org/biocLite.R'); \
    biocLite(pkgs=c('DESeq2','qvalue','multtest','org.EcK12.eg.db',\
    'genefilter','GEOquery','KEGG.db','metagenomeSeq','biomformat','phyloseq',\
    'GenABEL'))"

USER jovyan
# Install Bash Kernel
RUN pip install --user --no-cache-dir bash_kernel && \
    python -m bash_kernel.install
##------------------------------------------------------------
##------------------------------------------------------------
# Following to install python2, derived from https://github.com/jupyter/docker-stacks/blob/master/scipy-notebook/Dockerfile
USER $NB_USER

# Install Python 2 packages
# Remove pyqt and qt pulled in for matplotlib since we're only ever going to
# use notebook-friendly backends in these images
RUN conda create --quiet --yes -p $CONDA_DIR/envs/python2 python=2.7 \
    'ipython=4.2*' \
    && \
    conda clean -tipsy

# Add shortcuts to distinguish pip for python2 and python3 envs
RUN ln -s $CONDA_DIR/envs/python2/bin/pip $CONDA_DIR/bin/pip2 && \
    ln -s $CONDA_DIR/bin/pip $CONDA_DIR/bin/pip3

# Configure ipython kernel to use matplotlib inline backend by default
RUN mkdir -p $HOME/.ipython/profile_default/startup
# COPY mplimporthook.py $HOME/.ipython/profile_default/startup/


# Install qiime1 notebook as 
RUN conda create -n qiime1 python=2.7 qiime matplotlib=1.4.3 mock nose -c bioconda && \
    conda clean -tipsy
## conda install --quiet --yes -n python2 qiime matplotlib=1.4.3 mock nose -c bioconda && \
##    conda clean -tipsy
# RUN pip2 install qiime

USER root

RUN conda install --quiet --yes -n python2 --channel https://conda.anaconda.org/Biobuilds htseq pysam biopython

# add htseq-count to path
ENV PATH=${PATH}:$CONDA_DIR/envs/python2/bin

#--------------------------------------------------------
RUN echo "deb http://ftp.debian.org/debian unstable main" >  /etc/apt/sources.list.d/unstable.list && \
    aptitude update && \
    aptitude install --without-recommends --assume-yes -t unstable sra-toolkit
